import { Component } from '@angular/core';

export class News {

  constructor(public title: string,
              public body: string) {}

}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})


export class NewsComponent {

  newss: News[] = [];

  addNews(title: string, body: string) {
    this.newss.push(new News(title, body));
  }

}
